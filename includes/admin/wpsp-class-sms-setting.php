<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Public Pages Class
 * 
 * Handles all the different features of sms module
 * for the front end pages.
 * 
 * @package  WPSchoolPressSMS
 * @since 1.0.0
 */
class Wpspsms_Setting {
		
	function wpspsms_setting_html( $settings_data ) {
		ob_start(); ?>
			<form name="sms_settings_form" class="form-horizontal" method="post" id="sms_settings_form">
				<div class="form-group">
					<?php $smsprovider = isset( $settings_data['sch_sms_provider'] ) ? $settings_data['sch_sms_provider'] : ''; ?>
					<div class="col-md-2"><label><?php _e( 'SMS Provider', 'WPSchoolPress'); ?></label></div>										
					<div class="col-md-4">						
						<label for="smslane">  smsLane </label>
					</div>												
				</div>				
				<div class="sms_setting_div" id="sms_main_smslane">	
				<div class="form-group">
						<div class="col-md-2"><label><?php _e( 'Smslane ApiKey ', 'WPSchoolPress'); ?></label></div>
						<div class="col-md-4">
							<input type="text" name="sch_sms_slaneapikey" class="form-control" value="<?php echo isset( $settings_data['sch_sms_slaneapikey'] ) ? $settings_data['sch_sms_slaneapikey'] :''; ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2"><label><?php _e( 'Smslane Username', 'WPSchoolPress'); ?></label></div>
						<div class="col-md-4">
							<input type="text" name="sch_sms_slaneuser" class="form-control" value="<?php echo isset( $settings_data['sch_sms_slaneuser'] ) ? $settings_data['sch_sms_slaneuser'] :''; ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2"><label><?php _e( 'Smslane Password', 'WPSchoolPress');?></label></div>
						<div class="col-md-4">
							<input type="text" name="sch_sms_slanepassword" class="form-control" value="<?php echo isset( $settings_data['sch_sms_slanepassword'] ) ? $settings_data['sch_sms_slanepassword'] :''; ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2"><label><?php _e( 'Smslane sid', 'WPSchoolPress');?></label></div>
						<div class="col-md-4">
							<input type="text" name="sch_sms_slanesid" class="form-control" value="<?php echo isset( $settings_data['sch_sms_slanesid'] ) ? $settings_data['sch_sms_slanesid'] :''; ?>">
						</div>
					</div>
				</div>
				<input name="type" value="sms" type="hidden">
					<p><b>* <?php _e( 'Note:', 'WPSchoolPress'); ?></b>
						<?php _e( 'SMS settings will be done at the time of delivery based on your SMS provider API', 'WPSchoolPress'); ?></p>
					<button type="submit" class="btn btn-primary" name="submit" id="devesub">
					<i class="fa fa-save"></i><?php _e( 'Save', 'WPSchoolPress'); ?></button>
			</form>
	<?php
		$html = ob_get_clean();
		echo $html;
	}
	
	public function add_hooks() {		
		add_action( 'wpsp_sms_setting_html', array( $this, 'wpspsms_setting_html') );
		
	}
}