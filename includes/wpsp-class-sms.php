<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Public Pages Class
 * 
 * Handles all the different features of sms module
 * for the front end pages.
 * 
 * @package  WPSchoolPressSMS
 * @since 1.0.0
 */
class Wpspsms_Service {
	
	/*
	* Send sms to user
	* @package WPSchoolPressSMS
	* @since 2.0.0
	*/	
	function wpspsms_send_msg( $status, $receiver_number, $message_content ) {
		
		if( !empty( $receiver_number ) && !empty( $message_content) ) {
			
			global $wpdb, $wpsp_settings_data;			
			//smslane
				$user 		= isset( $wpsp_settings_data['sch_sms_slaneuser'] ) ? $wpsp_settings_data['sch_sms_slaneuser'] : ''; //smslane User
				$password 	= isset( $wpsp_settings_data['sch_sms_slanepassword'] ) ? $wpsp_settings_data['sch_sms_slanepassword'] : ''; // smslane Password
				$sid 		= isset( $wpsp_settings_data['sch_sms_slanesid'] ) ? $wpsp_settings_data['sch_sms_slanesid'] : ''; // smslane Sid	
				$apikey 	= isset( $wpsp_settings_data['sch_sms_slaneapikey'] ) ? $wpsp_settings_data['sch_sms_slaneapikey'] : ''; // smslane api key	
		
				if( !empty( $user ) && !empty( $password ) && !empty($sid) ) {
					$argument = array( 
						'apikey' => $apikey,
						'user' 		=> $user,
						'password' 	=> $password,
						'msisdn' 	=> '91'.$receiver_number, //to whom send $receiver_number
						'sid' 		=> $sid,
						'msg' 		=> $message_content,
						'fl' 		=> 0				
					);					
					$parameters = http_build_query( $argument );
				
					$response = wp_remote_get( "http://apps.smslane.com/vendorsms/pushsms.aspx?".$parameters, array( 'timeout'=>10 ) );				
					return true;	
				}
		}
		return false;
	}
	
	public function add_hooks() {
		
		add_filter( 'wpsp_send_notification_msg', array( $this, 'wpspsms_send_msg' ), 10, 3 );
	}
}