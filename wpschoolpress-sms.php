<?php
/*
Plugin Name: 	WPSchoolPress SMS Service
Plugin URI: 	http://wpschoolpress.com
Description:    Allow user to send sms using smslane
Version: 		1.0
Author: 		WpSchoolPress Team
Author URI: 	wpschoolpress.com/team
Text Domain:	WPSchoolPressPro
Domain Path:    languages
@package WPSchoolPressPro
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Basic plugin definitions
 * 
 * @package WPSchoolPressSMS
 * @since 1.0.0
*/
if( !defined( 'WPSPSMS_PLUGIN_URL' ) ) {
	define('WPSPSMS_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}

if( !defined( 'WPSPSMS_PLUGIN_PATH' ) ) {
	define( 'WPSPSMS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
}

if( !defined( 'WPSPSMS_PLUGIN_VERSION' ) ) {
	define( 'WPSPSMS_PLUGIN_VERSION', '1.0' ); //Plugin version number
}


add_action( 'plugins_loaded', 'wpspsms_plugins_loaded', 999 );
function wpspsms_plugins_loaded() {
	
		$wpsp_lang_dir	= dirname( plugin_basename( __FILE__ ) ) . '/languages/';
		load_plugin_textdomain( 'WPSchoolPresssms', false, $wpsp_lang_dir );		

		global $wpsp_settings_data, $wpspsms, $wpspsetting, $wpsp_sms_version, $wpsp_sms_version;
		
		//class handles most of functionalities of sending sms
		include_once( WPSPSMS_PLUGIN_PATH . 'includes/wpsp-class-sms.php' );
		$wpspsms = new Wpspsms_Service();
		$wpspsms->add_hooks();
		
		//class handles most of functionalities of sending sms
		include_once( WPSPSMS_PLUGIN_PATH . 'includes/admin/wpsp-class-sms-setting.php' );
		$wpspsetting = new Wpspsms_Setting();
		$wpspsetting->add_hooks();
		
		$wpsp_sms_version = new wpsp_sms_version();
		if( !class_exists('Wpsp_Admin') ) {
			add_action( 'admin_init', array( $wpsp_sms_version, 'wpspsms_plugin_deactivate' ) );
			add_action( 'admin_notices', array( $wpsp_sms_version, 'wpspsms_plugin_admin_notice' ) );
		}
}

class wpsp_sms_version {
	public function __construct() {
		
		add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'wpspsms_update_check' ) );
	}

	function wpspsms_plugin_admin_notice(){
		 echo '<div class="updated"><p> <strong> To Use  SMS Service, Please Activate WPSchoolPress Plugin</strong></p></div>';
         if ( isset( $_GET['activate'] ) )
            unset( $_GET['activate'] );
	}
	
	function wpspsms_plugin_deactivate() {
		 deactivate_plugins( plugin_basename( __FILE__ ) );
	}
	
	function wpspsms_update_check($transient) {

		// Check if the transient contains the 'checked' information
		// If no, just return its value without hacking it
		if ( empty( $transient->checked ) )
			return $transient;

		$plugin_path = plugin_basename( __FILE__ );

		// POST data to send to your API
		$args = array(
			'referrer' 	=> 	get_site_url(),
			'code' 		=>	get_option('wpsp-lcode')
		);

		// Send request checking for an update
		$response = $this->wpspsms_updateInfo( $args );
		$response=json_decode($response,true);

		$obj 				=	(object) array();
		$obj->slug 			=	'wpschoolpress-sms';
		$obj->new_version 	= 	$response['new_version'];
		$obj->tested 		=	$response['tested'];
		$obj->url 			= 	$response['url'];
		$obj->package 		=	$response['package'];
		// If there is a new version, modify the transient
		if( version_compare( $response['new_version'], $transient->checked[$plugin_path], '>' ) )
			$transient->response[$plugin_path] = $obj;
		return $transient;
	}
	
	function wpspsms_updateInfo( $args ) {
		// Send request
		$request = wp_remote_post('http://wpschoolpress.com/update/sms', array( 'method' => 'POST','body' => $args ) );
		if ( is_wp_error( $request ) || 200 != wp_remote_retrieve_response_code( $request ) )
			return false;
		return wp_remote_retrieve_body( $request ) ;
	}
}
